package ru.rus1f1kator.domainsample.ui;

import android.view.View;

/**
 * Created by kkharkov on 3/22/17.
 */

public interface OnItemClickListener {
    void onItemClick(View view, int position, long id);
}
