package ru.rus1f1kator.domainsample.ui;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.util.Log;

import java.util.List;

import ru.rus1f1kator.domainsample.cmd.Command;
import ru.rus1f1kator.domainsample.cmd.CommandStatus;
import ru.rus1f1kator.domainsample.cmd.MapSearch;
import ru.rus1f1kator.domainsample.content.Property;

class CmdLoader<P, R> extends AsyncTaskLoader<R> {

    private final Command<P, R> cmd;

    public CmdLoader(Context context, Command<P, R> cmd) {
        super(context);
        this.cmd = cmd;
        onContentChanged();
    }

    @Override
    public R loadInBackground() {
        Log.d("CmdLoader", "loadInBackground " + cmd);
        return cmd.execute();
    }

    @Override
    protected void onStartLoading() {
        if (takeContentChanged()) {
            forceLoad();
        }
    }

    @Override
    protected void onStopLoading() {
        cancelLoad();
    }

}
