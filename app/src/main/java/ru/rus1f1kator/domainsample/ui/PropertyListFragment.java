package ru.rus1f1kator.domainsample.ui;

import android.app.LoaderManager;
import android.content.Context;
import android.content.Loader;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.rus1f1kator.domainsample.R;
import ru.rus1f1kator.domainsample.cmd.CommandStatus;
import ru.rus1f1kator.domainsample.cmd.GetPropertiesList;
import ru.rus1f1kator.domainsample.cmd.MapSearch;
import ru.rus1f1kator.domainsample.content.Property;

public class PropertyListFragment extends Fragment implements LoaderManager.LoaderCallbacks<CommandStatus<List<Property>>>{

    private static final int LOADER_ID = 1;
    private PropertiesListHost hostCallback;
    private RecyclerView recyclerView;
    private PropertyAdapter propertyAdapter;
    private SwipeRefreshLayout swipeRefresh;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof PropertiesListHost) {
            hostCallback = (PropertiesListHost) context;
        } else {
            throw new IllegalStateException("Host activity must implement PropertiesListHost interface");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        hostCallback = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.property_list, container, true);
        swipeRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);
        swipeRefresh.setOnRefreshListener(new OnRefreshListener());
        recyclerView = (RecyclerView) view.findViewById(R.id.property_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        propertyAdapter = new PropertyAdapter(getActivity(), new PropertyClickListener());
        recyclerView.setAdapter(propertyAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        getActivity().getLoaderManager().initLoader(LOADER_ID, null, this);
        return view;
    }

    @Override
    public Loader<CommandStatus<List<Property>>> onCreateLoader(int id, Bundle args) {
        return new CmdLoader<>(getActivity(), new GetPropertiesList(new MapSearch.Param()));
    }

    @Override
    public void onLoadFinished(Loader<CommandStatus<List<Property>>> loader, CommandStatus<List<Property>> data) {
        List<Property> properties;
        if (CommandStatus.Ok.isOk(data)) {
            properties = ((CommandStatus.Ok<List<Property>>) data).getValue();
        } else {
            //TODO snackbar about something went wrong here
            properties = new ArrayList<>();
        }
        propertyAdapter.setItems(properties);
        swipeRefresh.setRefreshing(false);
    }

    @Override
    public void onLoaderReset(Loader<CommandStatus<List<Property>>> loader) {

    }

    public interface PropertiesListHost {
        void onPropertySelected(Property property);
    }

    private class PropertyClickListener implements OnItemClickListener {
        @Override
        public void onItemClick(View view, int position, long id) {
            if (hostCallback != null) {
                hostCallback.onPropertySelected(propertyAdapter.getProperty(position));
            }
        }
    }

    private class OnRefreshListener implements SwipeRefreshLayout.OnRefreshListener {
        @Override
        public void onRefresh() {
            getActivity().getLoaderManager().restartLoader(LOADER_ID, null, PropertyListFragment.this);
        }
    }
}
