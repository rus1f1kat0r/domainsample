package ru.rus1f1kator.domainsample.ui;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ru.rus1f1kator.domainsample.R;
import ru.rus1f1kator.domainsample.content.Property;

class PropertyAdapter extends RecyclerView.Adapter<PropertyAdapter.Holder> {

    private final Context context;
    private final LayoutInflater inflater;
    @NonNull
    private List<Property> items = new ArrayList<>();
    @Nullable
    private final OnItemClickListener listener;

    public PropertyAdapter(Context context, @Nullable OnItemClickListener listener) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.listener = listener;
        setHasStableIds(true);
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewType type = ViewType.valueOfElite(viewType);
        return type.createHolder(inflater.inflate(type.itemLayout, parent, false),
                context, listener);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        Property property = getProperty(position);
        holder.bind(property);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    @Override
    public int getItemViewType(int position) {
        int isElite = getProperty(position).getIsElite();
        /**
         * Can't just return isElite value. It might be extended at some point so we have to fall back to default.
         */
        return ViewType.valueOfElite(isElite).eliteValue;
    }

    @Override
    public long getItemId(int position) {
        return getProperty(position).getId();
    }

    public Property getProperty(int position) {
        return items.get(position);
    }

    public void setItems(@NonNull List<Property> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    static class Holder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private final ImageView thumb;
        private final TextView price;
        private final TextView address;
        private final TextView propertyParams;
        private final ImageView agencyLogo;

        final Context context;
        @Nullable
        final OnItemClickListener clickListener;

        Holder(View itemView, Context context,
               @Nullable OnItemClickListener clickListener) {
            super(itemView);
            price = (TextView) itemView.findViewById(R.id.price);
            address = (TextView) itemView.findViewById(R.id.address);
            propertyParams = (TextView) itemView.findViewById(R.id.property_params);
            thumb = (ImageView) itemView.findViewById(R.id.thumb1);
            agencyLogo = (ImageView) itemView.findViewById(R.id.agency_logo);
            this.context = context;
            this.clickListener = clickListener;
            itemView.setOnClickListener(this);
        }

        void bind(Property property) {
            price.setText(property.getDisplayPrice());
            address.setText(property.getDisplayableAddress());
            propertyParams.setText(context.getString(R.string.property_param_template,
                    property.getBedrooms(),
                    property.getBathrooms(),
                    property.getCarSpaces()));
            Glide.with(context)
                    .load(property.getThumbnailUrl())
                    .centerCrop()
                    .crossFade()
                    .into(thumb);
            Glide.with(context)
                    .load(property.getAgencyLogoUrl())
                    .fitCenter()
                    .crossFade()
                    .into(agencyLogo);
        }

        @Override
        public void onClick(View v) {
            if (clickListener != null) {
                clickListener.onItemClick(v, getAdapterPosition(), getItemId());
            }
        }
    }

    private static class EliteHolder extends Holder {
        private final ImageView thumb2;

        EliteHolder(View item, Context context, OnItemClickListener clickListener) {
            super(item, context, clickListener);
            this.thumb2 = (ImageView) item.findViewById(R.id.thumb2);
        }

        void bind(Property property) {
            super.bind(property);
            Glide.with(context)
                    .load(property.getThumbnailUrlSecond())
                    .centerCrop()
                    .crossFade()
                    .into(thumb2);
        }
    }

    private enum ViewType {
        DEFAULT(0, R.layout.property_list_item) {
            @Override
            Holder createHolder(View itemView, Context context, OnItemClickListener listener) {
                return new Holder(itemView, context, listener);
            }
        },
        ELITE(1, R.layout.property_list_item_elite) {
            @Override
            Holder createHolder(View itemView, Context context, OnItemClickListener listener) {
                return new EliteHolder(itemView, context, listener);
            }
        };
        private final int eliteValue;
        @LayoutRes
        private final int itemLayout;

        ViewType(int eliteValue, int itemLayout) {
            this.eliteValue = eliteValue;
            this.itemLayout = itemLayout;
        }

        abstract Holder createHolder(View itemView, Context context, OnItemClickListener listener);

        static ViewType valueOfElite(int eliteValue) {
            for(ViewType each : values()) {
                if (each.eliteValue == eliteValue) {
                    return each;
                }
            }
            return DEFAULT;
        }
    }

}
