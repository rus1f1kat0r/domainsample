package ru.rus1f1kator.domainsample.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import ru.rus1f1kator.domainsample.R;

public class PropertyDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    private long itemId;

    public PropertyDetailFragment() {
    }

    public static PropertyDetailFragment newInstance(long id) {
        Bundle args = new Bundle();
        args.putLong(PropertyDetailFragment.ARG_ITEM_ID, id);
        PropertyDetailFragment fragment = new PropertyDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey(ARG_ITEM_ID)) {
            /**
             * Load details for the property according to the its' id
             * We might want to use {@link android.content.Loader} API for that purpose.
             */
            itemId = arguments.getLong(ARG_ITEM_ID);

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
            if (appBarLayout != null) {
                appBarLayout.setTitle(String.valueOf(itemId));
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.property_detail, container, false);

        // Show the dummy content as text in a TextView.
        ((TextView) rootView.findViewById(R.id.property_detail)).setText(String.valueOf(itemId));
        return rootView;
    }
}
