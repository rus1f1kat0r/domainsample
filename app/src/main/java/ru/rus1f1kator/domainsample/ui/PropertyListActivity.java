package ru.rus1f1kator.domainsample.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import ru.rus1f1kator.domainsample.R;
import ru.rus1f1kator.domainsample.content.Property;

/**
 * An activity representing a list of properties. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link PropertyDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class PropertyListActivity extends AppCompatActivity implements PropertyListFragment.PropertiesListHost{

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean twoPane;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_content);

        if (findViewById(R.id.property_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            twoPane = true;
        }
    }

    @Override
    public void onPropertySelected(Property property) {
        if (twoPane) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.property_detail_container, PropertyDetailFragment.newInstance(property.getId()))
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commitNowAllowingStateLoss();
        } else {
            startActivity(new Intent(this, PropertyDetailActivity.class)
                    .putExtra(PropertyDetailFragment.ARG_ITEM_ID, property.getId()));
        }
        //Snackbar.make(getWindow().getDecorView(), "Property " + property.getId(), Snackbar.LENGTH_SHORT).show();
    }
}
