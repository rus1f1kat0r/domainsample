package ru.rus1f1kator.domainsample.content;

public enum ModeType {
    BUY("buy"),
    RENT("rent");

    private final String label;

    ModeType(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
