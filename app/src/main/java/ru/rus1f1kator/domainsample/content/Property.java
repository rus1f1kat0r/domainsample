package ru.rus1f1kator.domainsample.content;

public class Property {

    private long id;
    private String agencyLogoUrl;
    private int bathrooms;
    private int bedrooms;
    private int carSpaces;
    private String displayPrice;
    private String displayableAddress;
    private String truncatedDescription;
    private String thumbnailUrl; // was Retina prefix, wtf?
    private String thumbnailUrlSecond;
    /**
     * not boolean because it is business logic and we probably want to add some extra values
     * (even if we don't know about it by the moment)
     */
    private int isElite;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAgencyLogoUrl() {
        return agencyLogoUrl;
    }

    public void setAgencyLogoUrl(String agencyLogoUrl) {
        this.agencyLogoUrl = agencyLogoUrl;
    }

    public int getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(int bathrooms) {
        this.bathrooms = bathrooms;
    }

    public int getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(int bedrooms) {
        this.bedrooms = bedrooms;
    }

    public int getCarSpaces() {
        return carSpaces;
    }

    public void setCarSpaces(int carSpaces) {
        this.carSpaces = carSpaces;
    }

    public String getDisplayPrice() {
        return displayPrice;
    }

    public void setDisplayPrice(String displayPrice) {
        this.displayPrice = displayPrice;
    }

    public String getDisplayableAddress() {
        return displayableAddress;
    }

    public void setDisplayableAddress(String displayableAddress) {
        this.displayableAddress = displayableAddress;
    }

    public String getTruncatedDescription() {
        return truncatedDescription;
    }

    public void setTruncatedDescription(String truncatedDescription) {
        this.truncatedDescription = truncatedDescription;
    }

    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    public String getThumbnailUrlSecond() {
        return thumbnailUrlSecond;
    }

    public void setThumbnailUrlSecond(String thumbnailUrlSecond) {
        this.thumbnailUrlSecond = thumbnailUrlSecond;
    }

    public int getIsElite() {
        return isElite;
    }

    public void setIsElite(int isElite) {
        this.isElite = isElite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Property property = (Property) o;

        if (id != property.id) return false;
        if (bathrooms != property.bathrooms) return false;
        if (bedrooms != property.bedrooms) return false;
        if (carSpaces != property.carSpaces) return false;
        if (isElite != property.isElite) return false;
        if (agencyLogoUrl != null ? !agencyLogoUrl.equals(property.agencyLogoUrl) : property.agencyLogoUrl != null)
            return false;
        if (displayPrice != null ? !displayPrice.equals(property.displayPrice) : property.displayPrice != null)
            return false;
        if (displayableAddress != null ? !displayableAddress.equals(property.displayableAddress) : property.displayableAddress != null)
            return false;
        if (truncatedDescription != null ? !truncatedDescription.equals(property.truncatedDescription) : property.truncatedDescription != null)
            return false;
        if (thumbnailUrl != null ? !thumbnailUrl.equals(property.thumbnailUrl) : property.thumbnailUrl != null)
            return false;
        return thumbnailUrlSecond != null ? thumbnailUrlSecond.equals(property.thumbnailUrlSecond) : property.thumbnailUrlSecond == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (agencyLogoUrl != null ? agencyLogoUrl.hashCode() : 0);
        result = 31 * result + bathrooms;
        result = 31 * result + bedrooms;
        result = 31 * result + carSpaces;
        result = 31 * result + (displayPrice != null ? displayPrice.hashCode() : 0);
        result = 31 * result + (displayableAddress != null ? displayableAddress.hashCode() : 0);
        result = 31 * result + (truncatedDescription != null ? truncatedDescription.hashCode() : 0);
        result = 31 * result + (thumbnailUrl != null ? thumbnailUrl.hashCode() : 0);
        result = 31 * result + (thumbnailUrlSecond != null ? thumbnailUrlSecond.hashCode() : 0);
        result = 31 * result + isElite;
        return result;
    }
}
