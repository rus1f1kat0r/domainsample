package ru.rus1f1kator.domainsample.cmd;

import android.net.Uri;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import ru.rus1f1kator.domainsample.content.ModeType;
import ru.rus1f1kator.domainsample.content.Property;

public class MapSearch extends Command<MapSearch.Param, CommandStatus<List<Property>>> {

    private static final Uri BASE_URI = new Uri.Builder().scheme("https").authority("rest.domain.com.au").build();
    private static final String JSON_KEY_LISTING_RESULTS = "ListingResults";
    private static final String JSON_KEY_LISTINGS = "Listings";
    private static final String JSON_KEY_AD_ID = "AdId";
    private static final String JSON_KEY_AGENCY_LOGO_URL = "AgencyLogoUrl";
    private static final String JSON_KEY_BATHROOMS = "Bathrooms";
    private static final String JSON_KEY_BEDROOMS = "Bedrooms";
    private static final String JSON_KEY_CARSPACES = "Carspaces";
    private static final String JSON_KEY_DISPLAY_PRICE = "DisplayPrice";
    private static final String JSON_KEY_DISPLAYABLE_ADDRESS = "DisplayableAddress";
    private static final String JSON_KEY_TRUNCATED_DESCRIPTION = "TruncatedDescription";
    private static final String JSON_RETINA_DISPLAY_THUMB_URL = "RetinaDisplayThumbUrl";
    private static final String JSON_KEY_SECOND_RETINA_DISPLAY_THUMB_URL = "SecondRetinaDisplayThumbUrl";
    private static final String JSON_KEY_IS_ELITE = "IsElite";
    //TODO Application scope dependency
    private OkHttpClient httpClient;

    public MapSearch(Param params) {
        super(params);
        httpClient = new OkHttpClient.Builder()
                .retryOnConnectionFailure(true)
                .build();
    }

    @Override
    protected CommandStatus<List<Property>> onExecute() {
        Request request = new Request.Builder()
                .url(getUri().toString())
                .build();
        try {
            Response response = httpClient.newCall(request).execute();
            if (response.code() == 200) {
                return new CommandStatus.Ok<>(parseResponse(response));
            } else {
                return new CommandStatus.HttpError<>("http status code is " + response.code());
            }
        } catch (IOException e) {
            e.printStackTrace();
            return new CommandStatus.Error<>(e.getMessage());
        } catch (Throwable e) {
            return new CommandStatus.UnknownError<>(e.getMessage());
        }
    }

    private List<Property> parseResponse(Response response) throws IOException {
        JsonParser parser = null;
        try {
            parser = new ObjectMapper().getFactory().createParser(response.body().charStream());
            return getListFromResponseRoot(parser);
        } finally {
            if (parser != null) {
                try {
                    parser.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private List<Property> getListFromResponseRoot(JsonParser parser) throws IOException {
        JsonToken next = parser.nextToken();
        if (next == JsonToken.START_OBJECT) {
            while (parser.nextToken() != JsonToken.END_OBJECT) {
                String fieldName = parser.getCurrentName();
                parser.nextToken();
                if (JSON_KEY_LISTING_RESULTS.equals(fieldName)) {
                    return parseListingResults(parser);
                } else {
                    parser.skipChildren();
                }
            }
        } else {
            throw new JsonParseException(parser, "json object start expected");
        }
        throw new JsonParseException(parser, "field ListingResults expected");
    }

    private List<Property> parseListingResults(JsonParser parser) throws IOException {
        JsonToken next = parser.getCurrentToken();
        if (next == JsonToken.START_OBJECT) {
            while (parser.nextToken() != JsonToken.END_OBJECT) {
                String nextFieldName = parser.getCurrentName();
                next = parser.nextToken();
                if (JSON_KEY_LISTINGS.equals(nextFieldName)) {
                    if (next == JsonToken.START_ARRAY) {
                        List<Property> result = new ArrayList<>();
                        while (parser.nextToken() != JsonToken.END_ARRAY) {
                            result.add(parseProperty(parser));
                        }
                        return result;
                    } else {
                        throw new JsonParseException(parser, "json array start expected");
                    }
                } else {
                    parser.skipChildren();
                }
            }
        } else {
            throw new JsonParseException(parser, "json object start expected");
        }
        throw new JsonParseException(parser, "field Listings expected");
    }

    private Property parseProperty(JsonParser parser) throws IOException {
        Property result = new Property();
        if (parser.getCurrentToken() == JsonToken.START_OBJECT) {
            while (parser.nextToken() != JsonToken.END_OBJECT) {
                String fieldName = parser.getCurrentName();
                if (JSON_KEY_AD_ID.equals(fieldName)) {
                    result.setId(parser.nextLongValue(0));
                } else if (JSON_KEY_AGENCY_LOGO_URL.equals(fieldName)) {
                    result.setAgencyLogoUrl(parser.nextTextValue());
                } else if (JSON_KEY_BATHROOMS.equals(fieldName)) {
                    result.setBathrooms(parser.nextIntValue(0));
                } else if (JSON_KEY_BEDROOMS.equals(fieldName)) {
                    result.setBedrooms(parser.nextIntValue(0));
                } else if (JSON_KEY_CARSPACES.equals(fieldName)) {
                    result.setCarSpaces(parser.nextIntValue(0));
                } else if (JSON_KEY_DISPLAY_PRICE.equals(fieldName)) {
                    result.setDisplayPrice(parser.nextTextValue());
                } else if (JSON_KEY_DISPLAYABLE_ADDRESS.equals(fieldName)) {
                    result.setDisplayableAddress(parser.nextTextValue());
                } else if (JSON_KEY_TRUNCATED_DESCRIPTION.equals(fieldName)) {
                    result.setTruncatedDescription(parser.nextTextValue());
                } else if (JSON_RETINA_DISPLAY_THUMB_URL.equals(fieldName)) {
                    result.setThumbnailUrl(parser.nextTextValue());
                } else if (JSON_KEY_SECOND_RETINA_DISPLAY_THUMB_URL.equals(fieldName)) {
                    result.setThumbnailUrlSecond(parser.nextTextValue());
                } else if (JSON_KEY_IS_ELITE.equals(fieldName)) {
                    result.setIsElite(parser.nextIntValue(0));
                } else {
                    parser.nextToken();
                    parser.skipChildren();
                }
            }
        }
        if (result.getId() != 0) {
            return result;
        } else {
            throw new JsonParseException(parser, "At least id required for property");
        }
    }

    private Uri getUri() {
        return getParams().baseUri.buildUpon()
                .appendPath("searchservice.svc")
                .appendPath("mapsearch")
                .appendQueryParameter("mode", getParams().mode.getLabel())
                .appendQueryParameter("sub", getParams().suburb)
                .appendQueryParameter("pcodes", getParams().postalCodes)
                .appendQueryParameter("state", getParams().state)
                .build();
    }

    //TODO use code generation in order to avoid boilerplate
    public static class Param {
        private final Uri baseUri;
        private final ModeType mode;
        private final String suburb;
        private final String postalCodes;
        private final String state;

        public Param() {
            this(BASE_URI);
        }

        public Param(Uri baseUri) {
            this(baseUri, ModeType.BUY, "Bondi", "2062", "NSW");
        }

        public Param(Uri baseUri, ModeType mode, String suburb, String postalCodes, String state) {
            this.baseUri = baseUri;
            this.mode = mode;
            this.suburb = suburb;
            this.postalCodes = postalCodes;
            this.state = state;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Param param = (Param) o;

            if (mode != param.mode) return false;
            if (suburb != null ? !suburb.equals(param.suburb) : param.suburb != null) return false;
            if (postalCodes != null ? !postalCodes.equals(param.postalCodes) : param.postalCodes != null) return false;
            return state != null ? state.equals(param.state) : param.state == null;

        }

        @Override
        public int hashCode() {
            int result = mode != null ? mode.hashCode() : 0;
            result = 31 * result + (suburb != null ? suburb.hashCode() : 0);
            result = 31 * result + (postalCodes != null ? postalCodes.hashCode() : 0);
            result = 31 * result + (state != null ? state.hashCode() : 0);
            return result;
        }
    }
}
