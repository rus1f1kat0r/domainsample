package ru.rus1f1kator.domainsample.cmd;

import java.util.concurrent.Future;

public interface Executor {
    <V> Future<V> execute(Command<?, V> cmd) ;
}
