package ru.rus1f1kator.domainsample.cmd;
/**
 * Base Command pattern abstraction. Used for interactions to the
 * Web-server and local data base. Basically it is the simplest
 * element of work that can be performed. You can also compound
 * some {@link Command}s into the {@link CommandGroup} in order
 * to create sequences of actions that you are going to perform.
 * <br>
 * <br>
 * Subclasses should implement {@link #onExecute()} method in
 * order to perform some custom action and {@link #onDone()} method
 * in order to handle completion of the {@link Command}.
 * <br>
 * <br>
 * Command has {@link #cancelled} flag that indicates whether the
 * command has been cancelled or not. It is recommended to see
 * {@link #isCancelled()} while performing long running operations
 * and stop command execution as soon as possible.
 *
 * @author k.kharkov
 */
public abstract class Command<P, V>{
    /**
     * Command input parameters, immutable
     */
    private final P params;
    /**
     * Command output result, immutable
     */
    private V result;
    /**
     * Indicates whether cancellation has been requested over the command.
     */
    private boolean cancelled;
    /**
     * Creates an command that can be executed with {@link #execute()}
     * will be invoked.
     */
    public Command(P params) {
        super();
        this.cancelled = false;
        this.params = params;
    }
    /**
     * Called after execution has been requested. Should make
     * the command code to be run. For long running operations
     * you should always see {@link #isCancelled()} and stop the
     * command as soon as possible.
     */
    protected abstract V onExecute();
    /**
     * Starts command execution.
     * @see #onExecute()
     */
    public final V execute() {
        // execute if only the command has not been
        // cancelled before execution
        if (!isCancelled()) {
            setResult(onExecute());
            onDone();
            return result;
        }
        // Result(Canceled)
        return null;
    }

    public V getResult() {
        synchronized (this) {
            return result;
        }
    }

    protected void setResult(V result) {
        synchronized (this) {
            this.result = result;
        }
    }

    protected void onDone() {
    }
    /**
     * Requests cancellation of the command.
     * <br>
     * <b>Note:</b> There is no guarantee that the command
     * will be cancelled immediately or will be cancelled
     * at all. This method just requests the cancellation.
     * While implementing you should see {@link #isCancelled()}
     * in order to handle cancellation of the command.
     *
     * @see #isCancelled()
     */
    public void cancel() {
        synchronized (this) {
            cancelled = true;
        }
    }
    /**
     * See whether the cancellation has been requested.
     *
     * @return <code>true</code> in case cancellation
     * has been requested <code>false</code> otherwise.
     */
    public boolean isCancelled() {
        synchronized (this) {
            return cancelled;
        }
    }
    protected P getParams() {
        return params;
    }

    @Override
    public int hashCode() {
        int result = this.getClass().hashCode();
        result = 31 * result + (params != null ? params.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (params != null ? !params.equals(((Command) o).getParams()) : ((Command) o).getParams() != null)
            return false;
        return true;
    }
}
