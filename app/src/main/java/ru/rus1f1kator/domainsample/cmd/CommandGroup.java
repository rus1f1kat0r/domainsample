package ru.rus1f1kator.domainsample.cmd;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public abstract class CommandGroup<P, R> extends Command<P, R> {

    //TODO abstract out default implementation with locator or so
    private static Executor sExecutor = new ExecutorImpl();
    private final Executor executor = sExecutor;
    private Future<?> currentFuture;
    private Command<?, ?> currentCommand;

    public CommandGroup(P params) {
        super(params);
    }

    protected <V> V executeCommand(Command<?, V> cmd) throws InterruptedException{
        try {
            setCurrentCommand(cmd);
            if (isCancelled()) {
                throw new InterruptedException("cancelled");
            }
            Future<V> future = executor.execute(cmd);
            setCurrentFuture(future);
            return getResultFromFuture(future);
        } finally {
            setCurrentCommand(null);
            setCurrentFuture(null);
        }
    }

    private <V> V getResultFromFuture(Future<V> future) throws InterruptedException {
        try {
            V result = future.get();
            return result;
        } catch (ExecutionException e) {
            InterruptedException error = new InterruptedException();
            error.initCause(e);
            throw error;
        }
    }

    public static void setDefaultExecutor(Executor executor) {
        sExecutor = executor;
    }

    public Future<?> getCurrentFuture() {
        synchronized (this) {
            return currentFuture;
        }
    }

    public void setCurrentFuture(Future<?> currentFuture) {
        synchronized (this) {
            this.currentFuture = currentFuture;
        }
    }

    public Command<?, ?> getCurrentCommand() {
        synchronized (this) {
            return currentCommand;
        }
    }

    public void setCurrentCommand(Command<?, ?> currentCommand) {
        synchronized (this) {
            this.currentCommand = currentCommand;
        }
    }

    @Override
    public void cancel() {
        synchronized (this) {
            super.cancel();
            if (currentCommand != null) {
                currentCommand.cancel();
            }
            if (currentFuture != null) {
                currentFuture.cancel(true);
            }
        }
    }
}
