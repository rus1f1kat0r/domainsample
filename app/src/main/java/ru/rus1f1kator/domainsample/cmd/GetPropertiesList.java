package ru.rus1f1kator.domainsample.cmd;

import java.util.List;

import ru.rus1f1kator.domainsample.content.Property;

/**
 * Created on 3/28/17.
 */
public class GetPropertiesList extends CommandGroup<MapSearch.Param, CommandStatus<List<Property>>> {

    public GetPropertiesList(MapSearch.Param params) {
        super(params);
    }

    @Override
    protected CommandStatus<List<Property>> onExecute() {
        try {
            CommandStatus<List<Property>> listCommandStatus = executeCommand(new MapSearch(getParams()));
            // cache to DB or implement other requests etc
            return listCommandStatus;
        } catch (InterruptedException e) {
            return new CommandStatus.Error<>("Interrupted");
        }
    }
}
