package ru.rus1f1kator.domainsample;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import ru.rus1f1kator.domainsample.cmd.Command;
import ru.rus1f1kator.domainsample.cmd.CommandStatus;
import ru.rus1f1kator.domainsample.cmd.ExecutorImpl;
import ru.rus1f1kator.domainsample.cmd.MapSearch;
import ru.rus1f1kator.domainsample.content.Property;

/**
 * Created on 3/28/17.
 */
class MockExecutorImpl extends ExecutorImpl {

    List<Command<?, ?>> calls = new ArrayList<>();

    @Override
    public <V> Future<V> execute(Command<?, V> cmd) {
        calls.add(cmd);
        if (cmd.equals(new MapSearch(new MapSearch.Param()))) {
            return new Future<V>() {
                @Override
                public boolean cancel(boolean mayInterruptIfRunning) {
                    return false;
                }

                @Override
                public boolean isCancelled() {
                    return false;
                }

                @Override
                public boolean isDone() {
                    return true;
                }

                @Override
                public V get() throws InterruptedException, ExecutionException {
                    return (V) getListOk();
                }

                @Override
                public V get(long timeout, @NonNull TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
                    return (V) getListOk();
                }
            };
        } else {
            return super.execute(cmd);
        }
    }

    @NonNull
    public CommandStatus.Ok<List<Property>> getListOk() {
        ArrayList<Property> properties = new ArrayList<>();
        int count = 20 + calls.size();
        for (int i = 0; i < count; i++) {
            Property p = new Property();
            p.setId(i);
            p.setTruncatedDescription("prop " + i);
            p.setBathrooms(i);
            p.setBedrooms(i);
            p.setCarSpaces(i);
            p.setIsElite(i % 2);
            p.setDisplayableAddress("addr " + i);
            p.setDisplayPrice("$" + i);
            properties.add(p);
        }
        return new CommandStatus.Ok<List<Property>>(properties);
    }
}
