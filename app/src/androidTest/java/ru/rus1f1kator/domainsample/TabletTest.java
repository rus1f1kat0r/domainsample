package ru.rus1f1kator.domainsample;

import android.content.pm.ActivityInfo;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ru.rus1f1kator.domainsample.cmd.CommandGroup;
import ru.rus1f1kator.domainsample.ui.PropertyListActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.not;
import static ru.rus1f1kator.domainsample.PropertyListActivityTest.onRecyclerView;

/**
 * Created on 3/28/17.
 */
@RunWith(AndroidJUnit4.class)
public class TabletTest {

    static {
        CommandGroup.setDefaultExecutor(new MockExecutorImpl());
    }

    @Rule
    public IntentsTestRule<PropertyListActivity> rule = new IntentsTestRule<>(PropertyListActivity.class);

    @Before
    public void setUp() throws Exception {
        rule.getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    @Test
    public void testClickProperty() throws Exception {
        onView(withId(R.id.property_detail_container)).check(matches(allOf(
                isCompletelyDisplayed(),
                not(hasDescendant(isDisplayed()))
        )));
        onRecyclerView().perform(actionOnItemAtPosition(3, click()));
        onDetails().check(matches(withText("3")));
    }

    @Test
    public void testChangeDetails() throws Exception {
        testClickProperty();
        onRecyclerView().perform(actionOnItemAtPosition(18, click()));
        onDetails().check(matches(withText("18")));

    }

    public ViewInteraction onDetails() {
        return onView(withId(R.id.property_detail));
    }
}
