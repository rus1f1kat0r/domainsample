package ru.rus1f1kator.domainsample;

import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.test.runner.AndroidJUnit4;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import ru.rus1f1kator.domainsample.cmd.CommandStatus;
import ru.rus1f1kator.domainsample.cmd.MapSearch;
import ru.rus1f1kator.domainsample.content.Property;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(AndroidJUnit4.class)
public class MapSearchTest {

    private MockWebServer server;

    @Before
    public void setUp() throws Exception {
        server = new MockWebServer();
        server.start();
    }

    @After
    public void tearDown() throws Exception {
        server.shutdown();
    }

    @Test
    public void testResponseWithoutBody() throws Exception {
        server.enqueue(new MockResponse());
        CommandStatus<List<Property>> result = runDefaultCommand();
        assertThat(result, instanceOf(CommandStatus.Error.class));
    }

    private CommandStatus<List<Property>> runDefaultCommand() {
        return new MapSearch(new MapSearch.Param(Uri.parse(server.url("/").toString()))).execute();
    }

    @Test
    public void testNotOkHttpStatus() throws Exception {
        server.enqueue(new MockResponse().setResponseCode(500).setBody("{}"));
        CommandStatus<List<Property>> status = runDefaultCommand();
        assertThat(status, instanceOf(CommandStatus.HttpError.class));
    }

    @Test
    public void testOkWithAbsentFieldListingResults() throws Exception {
        server.enqueue(new MockResponse().setBody("{}"));
        CommandStatus<List<Property>> status = runDefaultCommand();
        assertThat(status, instanceOf(CommandStatus.Error.class));
    }

    @Test
    public void testWithAbsentFieldListing() throws Exception {
        server.enqueue(new MockResponse().setBody("{\"ListingResults\":{}}"));
        CommandStatus<List<Property>> status = runDefaultCommand();
        assertThat(status, instanceOf(CommandStatus.Error.class));
    }

    @Test
    public void testWithNotArrayInsideListing() throws Exception {
        server.enqueue(new MockResponse().setBody("{\"ListingResults\":{\"Listings\":{}}}"));
        CommandStatus<List<Property>> status = runDefaultCommand();
        assertThat(status, instanceOf(CommandStatus.Error.class));
    }

    @Test
    public void testEmptyArray() throws Exception {
        server.enqueue(new MockResponse().setBody("{\"ListingResults\":{\"Listings\":[]}}"));
        CommandStatus<List<Property>> status = runDefaultCommand();
        assertThat(status, instanceOf(CommandStatus.Ok.class));
        List<Property> properties = ((CommandStatus.Ok<List<Property>>) status).getValue();
        assertThat(properties.size(), is(0));
    }

    @Test
    public void testSingleProperty() throws Exception {
        server.enqueue(okWithSingleProperty());
        CommandStatus<List<Property>> status = runDefaultCommand();
        assertThat(status, instanceOf(CommandStatus.Ok.class));
        List<Property> properties = ((CommandStatus.Ok<List<Property>>) status).getValue();
        assertThat(properties.size(), is(1));
        Property property = properties.get(0);
        assertThat(property.getId(), is(100L));
        assertThat(property.getAgencyLogoUrl(), is("http://domain.com.au"));
        assertThat(property.getThumbnailUrl(), is("http://domain.com.au"));
        assertThat(property.getThumbnailUrlSecond(), is("http://domain.com.au"));
        assertThat(property.getIsElite(), is(1));
        assertThat(property.getBathrooms(), is(1));
        assertThat(property.getBedrooms(), is(2));
        assertThat(property.getCarSpaces(), is(1));
        assertThat(property.getDisplayableAddress(), is("Missenden road"));
        assertThat(property.getDisplayPrice(), is("Auction"));
        assertThat(property.getTruncatedDescription(), is("Smth awesome"));
    }

    @Test
    public void testPropertyWithOnlyId() throws Exception {
        server.enqueue(new MockResponse().setBody("{\"ListingResults\":{\"Listings\":[" +
                "{\"AdId\":100}" +
                "]}}"));
        CommandStatus<List<Property>> status = runDefaultCommand();
        assertThat(status, instanceOf(CommandStatus.Ok.class));
        List<Property> properties = ((CommandStatus.Ok<List<Property>>) status).getValue();
        assertThat(properties.size(), is(1));
        Property property = properties.get(0);
        assertThat(property.getId(), is(100L));
        assertThat(property.getAgencyLogoUrl(), nullValue());
        assertThat(property.getThumbnailUrl(), nullValue());
        assertThat(property.getThumbnailUrlSecond(), nullValue());
        assertThat(property.getIsElite(), is(0));
        assertThat(property.getBathrooms(), is(0));
        assertThat(property.getBedrooms(), is(0));
        assertThat(property.getCarSpaces(), is(0));
        assertThat(property.getDisplayableAddress(), nullValue());
        assertThat(property.getDisplayPrice(), nullValue());
        assertThat(property.getTruncatedDescription(), nullValue());
    }

    @NonNull
    private MockResponse okWithSingleProperty() {
        return new MockResponse().setBody("{\"ListingResults\":{\"Listings\":[" +
                "{\"AdId\":100,\"AgencyLogoUrl\":\"http://domain.com.au\",\"Bathrooms\":1,\"Bedrooms\":2,\"Carspaces\":1,\"DisplayPrice\":\"Auction\",\"DisplayableAddress\":\"Missenden road\",\"TruncatedDescription\":\"Smth awesome\",\"RetinaDisplayThumbUrl\":\"http://domain.com.au\",\"SecondRetinaDisplayThumbUrl\":\"http://domain.com.au\",\"IsElite\":1}" +
                "]}}");
    }
}
