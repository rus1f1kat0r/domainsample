package ru.rus1f1kator.domainsample;

import android.app.Instrumentation;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.RemoteException;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import ru.rus1f1kator.domainsample.cmd.CommandGroup;
import ru.rus1f1kator.domainsample.ui.PropertyDetailFragment;
import ru.rus1f1kator.domainsample.ui.PropertyListActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollToPosition;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtra;
import static android.support.test.espresso.intent.matcher.IntentMatchers.isInternal;
import static android.support.test.espresso.matcher.ViewMatchers.assertThat;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static junit.framework.Assert.fail;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;

/**
 * Created on 3/28/17.
 */
@RunWith(AndroidJUnit4.class)
public class PropertyListActivityTest {

    private static MockExecutorImpl executor;

    static {
        executor = new MockExecutorImpl();
        CommandGroup.setDefaultExecutor(executor);
    }

    @Rule
    public ActivityTestRule<PropertyListActivity> rule = new IntentsTestRule<>(PropertyListActivity.class, false, false);
    private Instrumentation.ActivityMonitor activityMonitor;

    @Before
    public void setup() {
        activityMonitor = new Instrumentation.ActivityMonitor(new IntentFilter(), null, false);
        InstrumentationRegistry.getInstrumentation().addMonitor(activityMonitor);
        rule.launchActivity(new Intent());
    }

    @After
    public void teardown() {
        InstrumentationRegistry.getInstrumentation().removeMonitor(activityMonitor);
    }

    @After
    public void tearDown() throws Exception {
        executor.calls.clear();
    }

    @Test
    public void testClickProperty() throws Exception {
        onRecyclerView().perform(actionOnItemAtPosition(3, click()));
        intended(allOf(
                isInternal(),
                hasExtra(PropertyDetailFragment.ARG_ITEM_ID, 3L),
                hasComponent("ru.rus1f1kator.domainsample.ui.PropertyDetailActivity")
        ));
    }

    @Test
    public void test20Items() throws Exception {
        onRecyclerView().perform(scrollToPosition(20));
    }

    @Test
    public void testAddress() throws Exception {
        onRecyclerView().perform(scrollToPosition(12));
        onRecyclerView().check(matches(hasDescendant(allOf(
                withId(R.id.address), withText("addr 12"))
        )));
    }

    @Test
    public void testPrice() throws Exception {
        onRecyclerView().perform(scrollToPosition(13));
        onRecyclerView().check(matches(hasDescendant(allOf(
                withId(R.id.price), withText("$13"))
        )));
    }

    @Test
    public void testPropertyParams() throws Exception {
        onRecyclerView().perform(scrollToPosition(14));
        onRecyclerView().check(matches(hasDescendant(allOf(
                withId(R.id.property_params), withText("14 bed, 14 bath, 14 car"))
        )));
    }

    @Test
    public void testPullToRefreshWillAddOneItem() throws Exception {
        onRecyclerView().perform(swipeDown());
        onRecyclerView().perform(scrollToPosition(21));
    }

    /**
     * we mock data in a way that elite and standard are following one by one.
     * @see MockExecutorImpl
     */
    @Test
    public void testIsElite() throws Exception {
        for (int i = 0; i < 20; i ++) {
            onRecyclerView().perform(scrollToPosition(i));
            onView(RecyclerViewMatcher.withRecyclerView(R.id.property_list).atPosition(i)).check(matches(
                    not(hasDescendant(withId(R.id.thumb2)))
            ));
            i++;
            onRecyclerView().perform(scrollToPosition(i));
            onView(RecyclerViewMatcher.withRecyclerView(R.id.property_list).atPosition(i)).check(matches(
                    hasDescendant(withId(R.id.thumb2))
            ));
        }
    }

    @Test
    public void testOrientationChangeDoesntRefetchList() throws Exception {
        onRecyclerView().perform(scrollToPosition(8));
        rotateDevice();
        onRecyclerView().perform(scrollToPosition(9));
        rotateDevice();
        assertThat(executor.calls.size(), is(1));
    }

    public static void rotateDevice() {
        try {
            UiDevice device = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
            device.unfreezeRotation();
            if (device.isNaturalOrientation()) {
                device.setOrientationRight();
            } else {
                device.setOrientationNatural();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
            fail();
        }
        onView(isRoot()).check(matches(isDisplayed()));
    }

    public static ViewInteraction onRecyclerView() {
        return onView(withId(R.id.property_list));
    }
}
