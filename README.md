# README #

## Structure ##
* ui package includes activities, fragments and all the other user interface related classes
* content package defines model for the project that is generally Property POJO declaration
* cmd package defines and implements base unit of work pattern for the application that takes over all long running operation handling.

## How to run ##
Domain sample is a gradle project, so could be built with 
```
./gradlew assemble
```

## How to run tests ##
In the sample project there are very basic tests that verifies MVP for domain sample project

* unit tests for map search cmd
* ui tests for property list activity are divided into two groups: one for the device with smallest width less than 600dp (PropertyListActivityTest), the other for the devices with smallest width 600dp+ (TabletTest)

## Architecture considerations ##

### Cmd and unit of work ###


In rather big android applications that follow best practices and architecture design guidelines there is usually a situation where component independence and decoupling (encouraged by using fragments, scoped activities etc) leads to work duplication because some parts might depend on same data that have to be fetched from remote resources. Executor solves this problem by returning the future for the work that has been requested (originally defined in "Java concurrency in practice" book). Also it allows to define multiple thread pools for different types of work in order not to block the work that could be performed locally because we have full queue of network tasks.

### Recycler view Adapter###


In order to support multiple view types and avoid duplication of yourself (if/else or switch case) we use enum that points to different view holders types. The view holder itself is an active object that knows how to bind data to the view. This pattern allows to extend easily and support view type extension.

### Orientation changes ###

In order to solve this problem it is common pattern to rely on LoaderManager API that is supposed to preserve loader across different configuration instances.